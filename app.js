require('dotenv').config();
const express = require("express");
const logger = require("morgan");
const PORT = process.env.PORT || 3000;
const app = express();
const json = require('./public/data')

app.use(logger("dev"));

app.set("views", "./src/views");
app.set("view engine", "pug");
app.use("/static", express.static("./public"));
// const booksList = require('./bookslist.json')
// const item = require('./item.json')
// const product = require('./products.json')
// const productId = require('./productId.json')


app.get("/", (request, response) => {
    response.render("main", {
        projectTitle: "API Docs of Books"

    });
});

app.get("/docs/books/list", (request, response) => {
    response.render("booksList", {
        projectTitle: "GET /books",
        data: json.booklist,
        endpoint: "/docs/books/list"

    });
})
app.get('/docs/books/item', (request, response) => {
    response.render("item", {
        projectTitle: "GET /books/:id",
        data: json.item

    });
});
app.get("/docs/products/list", (request, response) => {
    response.render("list", {
        projectTitle: "GET /products",
        data: json.products

    });
});

app.get("/docs/products/list", (request, response) => {
    response.render("productId", {
        projectTitle: "GET /products/:id",
        data: json.productId

    });
});

app.listen(PORT, () => {
    console.log(`Running on PORT ${PORT}`)
});
